Source: minimap-el
Section: editors
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Lev Lamberov <dogsleg@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-elpa
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://elpa.gnu.org/packages/minimap.html
Vcs-Browser: https://salsa.debian.org/emacsen-team/minimap-el
Vcs-Git: https://salsa.debian.org/emacsen-team/minimap-el.git

Package: elpa-minimap
Architecture: all
Depends: ${elpa:Depends},
         ${misc:Depends}
Recommends: emacs (>= 46.0)
Enhances: emacs
Description: sidebar showing a "mini-map" of a buffer
 This package is an implementation of a minimap sidebar, i.e., a
 smaller display of the current buffer on the left side.  It
 highlights the currently shown region and updates its position
 automatically.  You can navigate in the minibar by dragging the
 active region with the mouse, which will scroll the corresponding
 edit buffer. Additionally, you can overlay information from the tags
 gathered by CEDET's semantic analyzer.
